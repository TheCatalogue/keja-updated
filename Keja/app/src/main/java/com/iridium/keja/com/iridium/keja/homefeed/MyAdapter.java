package com.iridium.keja.com.iridium.keja.homefeed;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iridium.keja.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>
{
    private List<ListItem> listItems;
    private Context contex;

    public MyAdapter(List<ListItem> listItems,Context contex) {
        this.listItems = listItems;
        this.contex = contex;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item,viewGroup,false);
        return  new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i)
    {
        ListItem item = listItems.get(i);

        viewHolder.houseName.setText(item.getHouseName());
        viewHolder.numOfViews.setText(item.getNumViews());
   //     Picasso.get().load(item.getHouseImage()).into(viewHolder.myImage);
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView numOfViews;
        public TextView houseName;
        public ImageView myImage;
        public ImageView profileImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            numOfViews = itemView.findViewById(R.id.views_text);
            houseName = itemView.findViewById(R.id.profile_name);
            myImage = itemView.findViewById(R.id.displayImage);
            profileImage = itemView.findViewById(R.id.profile_pic);
        }
    }

}
