package com.iridium.keja.com.iridium.keja.signup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.iridium.keja.R;
import com.iridium.keja.com.iridium.keja.homefeed.HomeActivity;
import com.iridium.keja.com.iridium.keja.login.LoginActivity;

import java.util.HashMap;
import java.util.Map;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonRegister;
    private EditText editTextFullName;
    private EditText editTextPhoneNumber;
    private EditText editTextEmail;
    private EditText getEditTextPassword;
    private TextView textViewSignin;

    private ProgressDialog progressDialog;

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener  firebaseAuthListener;
    private FirebaseFirestore mFirestore;

// ...




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);


        // Initializing views
        progressDialog = new ProgressDialog(this);
        buttonRegister = (Button) findViewById(R.id.buttonRegister);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPhoneNumber = (EditText) findViewById(R.id.editTextPhoneNumber);
        getEditTextPassword= (EditText) findViewById(R.id.editTextPassword);
        editTextFullName = (EditText) findViewById(R.id.editTextFullName);
        textViewSignin = (TextView) findViewById(R.id.textViewSignin);

        // attaching listeners to buttons
        textViewSignin.setOnClickListener(this);
        buttonRegister.setOnClickListener(this);

        firebaseAuth = firebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();


    }
    private void registerUser() {
        final String name = editTextFullName.getText().toString();
        final String phone = editTextPhoneNumber.getText().toString();
        String email = editTextEmail.getText().toString();
        String password = getEditTextPassword.getText().toString();

        if (TextUtils.isEmpty(email)){
            // email is empty
            Toast.makeText(this, "Please enter your email", Toast.LENGTH_SHORT).show();
            return;
        }if (TextUtils.isEmpty(name)){
            // name is empty
            Toast.makeText(this, "Please enter your name", Toast.LENGTH_SHORT).show();
            return;
        }if (TextUtils.isEmpty(phone)){
            // phone is empty
            Toast.makeText(this, "Please enter your phone number", Toast.LENGTH_SHORT).show();
            return;
        }if (TextUtils.isEmpty(password)){
            // password is empty
            Toast.makeText(this, "Please enter a password", Toast.LENGTH_SHORT).show();
            return;
        }
        // after validations are true
        // show a progress dialog if email and password are not empty
        progressDialog.setMessage("Just a moment...");
        progressDialog.show();

        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(SignupActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(SignupActivity.this, "Error. Please try again", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            // mapping values to be stored in the database
                            Map<String, Object> user = new HashMap<>();
                            user.put("name", name);
                            user.put("phone", phone);

                            mFirestore.collection("users").add(user);
                        }
                    }
                });

    }

    // method to show message
    private void showMessage(String message) {
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
    }


    @Override
    public void onClick(View view) {
        if(view == buttonRegister) {
            registerUser();
            toNextActivity(HomeActivity.class);
        }

        // opens login activity
        if(view == textViewSignin) {
            toNextActivity(LoginActivity.class);

        }
    }

    private void toNextActivity(Class myActivity) {
        Intent intent = new Intent(this, myActivity);
        startActivity(intent);
    }


}
