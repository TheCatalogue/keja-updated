package com.iridium.keja.com.iridium.keja.notification;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.iridium.keja.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<NotifItem> notifications;

    private static final String URL_DATA= "https://vast-brushlands-23089.herokuapp.com/main/api/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        getSupportActionBar().hide();

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        notifications = new ArrayList<>();

        loadData();
    }

    private void loadData()
    {
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage("Loading data ...");
        progress.show();

        StringRequest request = new StringRequest(Request.Method.GET,URL_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progress.dismiss();
                try {

                    JSONArray array = new JSONArray(response);

                    for(int c =0;c <array.length();c++)
                    {
                        JSONObject o = array.getJSONObject(c);

                        String myNotification = o.getString("description");
                        String profImage = o.getString("image");

                        NotifItem item = new NotifItem(myNotification,profImage);
                        notifications.add(item);
                    }

                    adapter = new NotificationAdapter(notifications,getApplicationContext());
                    recyclerView.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();

            }
        });

        RequestQueue rq = Volley.newRequestQueue(this);
        rq.add(request);
    }
}
