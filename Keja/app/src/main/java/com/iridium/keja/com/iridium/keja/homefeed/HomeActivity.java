package com.iridium.keja.com.iridium.keja.homefeed;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.iridium.keja.com.iridium.keja.new_house.NewHouse;
import com.iridium.keja.R;
import com.iridium.keja.com.iridium.keja.notification.NotificationActivity;
import com.iridium.keja.com.iridium.keja.profile.ProfileActivity;
import com.iridium.keja.com.iridium.keja.search.SearchActivity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HomeActivity extends AppCompatActivity
{

    DatabaseReference reference;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<ListItem> listItems;
    private String rent;
    private String name ;
    private String image;
    private String views;


    private final String TAG = "HomeActivity" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        Log.d(TAG,"ON CREATE METHOD CALLED .....");
        FloatingActionButton fab =  findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                toNextActivity(NewHouse.class);

                /*
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            */
            }
        });


        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        loadData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return true;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_search:
                toNextActivity(SearchActivity.class);
                return true;
            case R.id.action_notif:
                toNextActivity(NotificationActivity.class);
                return true;
            case R.id.action_profile:
                toNextActivity(ProfileActivity.class);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void toNextActivity(Class myActivity) {
        Intent intent = new Intent(this, myActivity);
        startActivity(intent);
    }


    private void loadData()
    {
        reference = FirebaseDatabase.getInstance().getReference().child("users").child("Solomon").child("houses").child("langata").child("lazarus");
        reference.addValueEventListener(new ValueEventListener()

        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                Log.d(TAG,"ON CREATE METHOD CALLED .....");
                listItems = new ArrayList<>();
                List <String>myList = new ArrayList<>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren())
                {
                    myList.add(dataSnapshot1.getValue(String.class));
                }

                Iterator<String> iterator = myList.iterator();
                while(iterator.hasNext())
                {
                    name = iterator.next();
                    rent = iterator.next();
                    views = iterator.next();

                }
                ListItem item = new ListItem(name,views);
                listItems.add(item);

                adapter = new MyAdapter(listItems, getApplicationContext());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });

        final ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage("Loading data ...");
        progress.show();

    }
}
