package com.iridium.keja.com.iridium.keja.notification;

public class NotifItem {
    private  String notification;
    private String image;

    public NotifItem(String notification,String image)
    {
        this.notification =notification;
        this.image = image;
    }

    public String getNotif()
    {
        return notification;
    }

    public String getImage(){return image;}

    public void setImage(String image)
    { this.image = image;}

}
