package com.iridium.keja.com.iridium.keja.search;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.iridium.keja.R;

public class SearchActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getSupportActionBar().hide();
        
    }
    private void toNextActivity(Class myActivity)
    {
        Intent intent = new Intent(this,myActivity);
        startActivity(intent);
    }
}
