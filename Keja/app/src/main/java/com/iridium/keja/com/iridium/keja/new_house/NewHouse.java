package com.iridium.keja.com.iridium.keja.new_house;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.iridium.keja.R;
import com.iridium.keja.com.iridium.keja.homefeed.HomeActivity;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;


public class NewHouse extends AppCompatActivity {

    private Spinner houseType;
    private Spinner numOfUnits;
    private EditText houseName;
    private EditText houseLocation;
    private CheckBox internet;
    private EditText rent;
    private ImageView new_hse_imageview;
    private Button addNewEntry;
    private Button backFromNewEntry;

    private Bitmap bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath = null;
    private static final int PICK_IMAGE_ID = 234;
    private ProgressBar mProgressDialog;

    private StorageReference mStorageRef;
    private FirebaseAuth auth;
    private ArrayList<String> pathArray;
    private int array_position;
    private  Uri selectedImage;



    private String TAG = "NewHouse";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_house);
        getSupportActionBar().hide();

        initialize();

        String[] hsetypes = new String[]{"BedSitter","1 Bedroom","2 Bedroom","3 Bedroom","4 Bedroom"};
        ArrayAdapter<String> type_adapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,hsetypes);
        houseType.setAdapter(type_adapter);


        String[] units = new String[]{"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"};
        ArrayAdapter<String> units_adapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,units);
        numOfUnits.setAdapter(units_adapter);

        new_hse_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, 0);//zero can be replaced with any action code

                Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 1);//one can be replaced with any action code

            }
        });

        addNewEntry.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                uploadData();
            }
        });

        backFromNewEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toNextActivity(HomeActivity.class);
            }
        });
    }

    private void initialize()
    {
        houseName = findViewById(R.id.new_hse_name);
        rent = findViewById(R.id.new_hse_rent);
        internet = findViewById(R.id.internet);
        new_hse_imageview = findViewById(R.id.new_hse_image);
        addNewEntry = findViewById(R.id.new_hse_submit_btn);
        houseType = findViewById(R.id.hse_type);
        numOfUnits = findViewById(R.id.num_units);
        mProgressDialog = findViewById(R.id.progressBar);
        backFromNewEntry = findViewById(R.id.back_from_new_house);

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent)
    {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case 0:
                if(resultCode == RESULT_OK){
                    selectedImage = imageReturnedIntent.getData();
                    new_hse_imageview.setImageURI(selectedImage);
                }

                break;
            case 1:
                if(resultCode == RESULT_OK){
                    selectedImage = imageReturnedIntent.getData();
                    new_hse_imageview.setImageURI(selectedImage);
                }
                break;
        }
    }

    private void uploadData()
    {
        Log.d(TAG, "onClick: Uploading Image.");


        //get the signed in user
        FirebaseUser user = auth.getCurrentUser();
        String userID = user.getUid();

            Uri uri = selectedImage;
            StorageReference storageReference = mStorageRef.child("images/users/" + userID + ".jpg");
            storageReference.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // Get a URL to the uploaded content
          //          Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    toastMessage("Upload Success");
             //       mProgressDialog.dismiss();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    toastMessage("Upload Failed");
             //       mProgressDialog.dismiss();
                }
            })
            ;
        }

    private void toastMessage(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    private void toNextActivity(Class myActivity) {
        Intent intent = new Intent(this, myActivity);
        startActivity(intent);
    }

}
