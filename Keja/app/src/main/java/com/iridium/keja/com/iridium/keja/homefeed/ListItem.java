package com.iridium.keja.com.iridium.keja.homefeed;

public class ListItem {
    private  String name;
    private String views;
    private String image;

    public ListItem(String name,String views)
    {
        this.name = name;
        this.views = views;
        this.image = image;
    }

    public void setHouseName(String name)
    {
        this.name =name;
    }
    public void setNumViews(String views)
    {
        this.views = views;

    }
    public void setImage(String image)
    {
        this.image = image;
    }


    public String getHouseName()
    {
        return name;
    }

    public String getHouseDesc()
    {
        return views;
    }

    public String getHouseImage() {
        return image;
    }

    public void setHouseImage(String image) {
        this.image = image;
    }

    public String getNumViews(){return views;}
}
