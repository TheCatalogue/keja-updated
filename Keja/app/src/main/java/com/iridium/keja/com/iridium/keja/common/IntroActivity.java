package com.iridium.keja.com.iridium.keja.common;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.FirebaseApp;
import com.iridium.keja.R;
import com.iridium.keja.com.iridium.keja.homefeed.HomeActivity;
import com.iridium.keja.com.iridium.keja.login.LoginActivity;


public class IntroActivity extends AppCompatActivity {

    private static final int SPLASH_TIMEOUT = 2500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        FirebaseApp.initializeApp(this);

        setContentView(R.layout.activity_intro);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run()
            {
                Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }

        },SPLASH_TIMEOUT);

    }

}
